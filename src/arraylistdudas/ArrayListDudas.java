package arraylistdudas;

import java.util.ArrayList;

public class ArrayListDudas {

    public static void main(String[] args) {
        System.out.println("Hola");
        ////////////////////////
        //PRIMER EJEMPLO
        // Agregar items al arraylist
        ////////////////////////

        //declaramos
        ArrayList listado;
        listado = new ArrayList();

        String cliente1 = "Laura";
        String cliente2 = "Carlos";
        String cliente3 = "Luis";
        String cliente4 = "Emanuel";
        String cliente5 = "Ariel";

        listado.add(cliente1);
        listado.add(cliente2);
        listado.add(cliente3);
        listado.add(cliente4);
        listado.add(cliente5);

        System.out.println("1er ejemplo" + listado);

        ////////////////////////
        //SEGUNDO EJEMPLO
        //Agregar una categoria a los productos
        ////////////////////////
        //Alta Categorias
        Categorias miCategoria1 = new Categorias();
        miCategoria1.nombre = "Electronica";
        Categorias miCategoria2 = new Categorias();
        miCategoria2.nombre = "Muscia";
        Categorias miCategoria3 = new Categorias();
        miCategoria3.nombre = "Muebles";

        //Alta productos
        ArrayList listadoProductos = new ArrayList();
        Productos miProducto1 = new Productos();
        miProducto1.nombre = "Samsung Slim";
        //miProducto1.categoria = miCategoria1;
        Productos miProducto2 = new Productos();
        miProducto2.nombre = "Guitarra";
        //miProducto2.categoria = miCategoria2;
        Productos miProducto3 = new Productos();
        miProducto3.nombre = "Motorola Moto";
        //miProducto3.categoria = miCategoria1;
        Productos miProducto4 = new Productos();
        //miProducto4.categoria = miCategoria2;
        miProducto4.nombre = "Flauta";

        //Agregar productos al listado
        listadoProductos.add(miProducto1);
        listadoProductos.add(miProducto2);
        listadoProductos.add(miProducto3);
        listadoProductos.add(miProducto4);
        System.out.println("2do ejemplo. " + listadoProductos);

        ////////////////////////
        //TERCER EJEMPLO
        //Que los productos tengan mas de una categoria
        ////////////////////////
        Productos miProducto5 = new Productos();
        miProducto5.nombre = "Xioami";
        miProducto5.categoria.add(miCategoria3);
        miProducto5.categoria.add(miCategoria2);
        System.out.println("3er ejemplo " + miProducto5);
        
        
        ////////////////////////
        //CUARTO EJEMPLO
        //UN ticket que calcule el precio unitario por la cantidad
        ////////////////////////
        Productos miProducto6 = new Productos();
        miProducto6.nombre = "Alcatel";
        miProducto6.categoria.add(miCategoria3);
        miProducto6.categoria.add(miCategoria2);
        miProducto6.precio = 15000;
        miProducto6.cantidad = 3;
        System.out.println("4to ejemplo");
        miProducto6.calcularTotal();

        
        System.out.println("Chau");
    }

}
